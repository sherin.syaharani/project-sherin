var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mysql = require('mysql');

var dbConn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'program_lanjutan'
});
// connect to database
dbConn.connect(); 

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// default route
app.get('/', function (req, res) {
    return res.send({ error: true, message: 'hello' })
});

//ngambil semua users
app.get('/users', function (req, res) {
    dbConn.query('SELECT * FROM users', function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'users list.' });
    });
});

// set port
app.listen(3000, function () {
    console.log('Node app is running on port 3000');
});
